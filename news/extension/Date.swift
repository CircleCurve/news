//
//  String.swift
//  news
//
//  Created by Patrick Ng on 4/4/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//
import Foundation
extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let myString = formatter.string(from: self)
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = format
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        return myStringafd
    }
    
}
