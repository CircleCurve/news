//
//  UIImageView.swift
//  news
//
//  Created by Patrick Ng on 29/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//

import UIKit
extension UIImageView {
    func downloadImageFrom(link:String, contentMode: UIViewContentMode) {
        URLSession.shared.dataTask( with: NSURL(string:link)! as URL, completionHandler: {
            (data, response, error) -> Void in
            DispatchQueue.main.async() {
                self.contentMode =  contentMode
                if let data = data { self.image = UIImage(data: data) }
            }
        }).resume()
    }
}
