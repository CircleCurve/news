//
//  RealmMigration.swift
//  news
//
//  Created by Patrick Ng on 27/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//

import Foundation
import RealmSwift

class RealmMigration {
    
    
    func didApplicationLunch () {
        self.migrationVersion()
    }
    
    //MARK: Realm Migrations
    
    func migrationVersion() {
        
        let config = Realm.Configuration(
            schemaVersion : 1 ,
            migrationBlock : { migration , oldSchemaVersion in
                migration.enumerateObjects(ofType: RLM_RSS.className(),{ oldObject, newObject in
                    // make sure to check the version accordingly
                    if (oldSchemaVersion < 2) {
                        // the magic happens here: `id` is the property you specified
                        // as your primary key on your Model
                        newObject!["uuid"] = UUID().uuidString
                    }
                    }
                )}
        )
        
        Realm.Configuration.defaultConfiguration = config
    
    }
}
let realm = try! Realm()
