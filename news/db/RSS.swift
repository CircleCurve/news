//
//  RSS.swift
//  news
//
//  Created by Patrick Ng on 21/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//

import Foundation
import RealmSwift

class RSS {
    
    static func save(link: String, title: String, desc: String) -> RLM_RSS {
        let rssItem = RLM_RSS()
        rssItem.link = link
        rssItem.title = title
        rssItem.desc = desc
        
        try! realm.write {
            realm.add(rssItem)
            print ("DB File save in \(Realm.Configuration.defaultConfiguration.fileURL!)")
        }
        
        return rssItem
    }
    
    static func update(feed: RLM_RSS, _ arr: [String:String]) {
        
        try! realm.write {
            feed.link = arr["link"] ?? ""
            feed.title = arr["title"] ?? ""
            feed.desc = arr["desc"] ?? ""
            print("Update success")
        }
    }
    
    static func remove (feed: RLM_RSS) {
        try! realm.write {
            realm.delete(feed)
            print ("Delete success. DB File save in \(Realm.Configuration.defaultConfiguration.fileURL!)")
        }
    }
    
    static func all() -> [RLM_RSS] {
        //let realm = try! Realm()
        let rssItem = realm.objects(RLM_RSS.self)
        return Array(rssItem)
    }
    
    
    
    static func count() -> Int {
        let rssItem = realm.objects(RLM_RSS.self)
        return rssItem.count
    }
    
    static func fetch(url: String, completion: @escaping (_ results : [Any])->Void ) {
        //let url = "https://www.gameapps.hk/rss"
        //let url = "https://developer.apple.com/news/rss/news.rss"
        //Networking.request(url: url, completion: { ( results : [Any] ) in
        
        Networking.request(
                url: url,
                onSuccess: { status, msg, results in
                    print("success to call rss")
                    for result in results {
                        print (result)
                    }
                    completion(results)
                    //totalResults.append(results)
            },
                onFail: { status, msg in
                    print("fail to call rss, status:\(status), msg:\(msg)")
            },
                onError: { status, msg in
                    print("network error to call rss, status:\(status) msg:\(msg)")
            }
            )
        }
        
}
