//
//  RSSItem.swift
//  news
//
//  Created by Patrick Ng on 23/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//

import RealmSwift
class RLM_RSS : Object {
    @objc private(set) dynamic var uuid:String = UUID().uuidString
    
    @objc dynamic var link = ""
    @objc dynamic var title = ""
    @objc dynamic var desc = ""
    
    //設置索引主鍵
    override static func primaryKey() -> String {
        return "uuid"
    }
}
