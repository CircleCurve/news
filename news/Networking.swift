//
//  Networking.swift
//  news
//
//  Created by Patrick Ng on 19/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//

import Foundation
import PromiseKit
import Alamofire
import AlamofireRSSParser

class Networking {
    
    static func request(
        url: String,
        onSuccess: @escaping (_ status: String, _ msg: String,_ result: [Any] )->Void,
        onFail: @escaping (_ status: String, _ msg: String )->Void,
        onError: @escaping (_ status: String, _ msg: String )->Void) {
        
        Alamofire.request(url)
            .responseRSS { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            switch response.result {
            case .success:
                guard let feed: RSSFeed = response.result.value  else {
                    onFail("-1", "wrong format")
                    return
                }
                onSuccess("1", "success to get result", feed.items)
            case .failure(let error):
                onError( String (response.response?.statusCode ?? 1001) , error.localizedDescription)
                print(error)
            }
            
        }
    }
    
    func requestChain(url: String) -> Promise<[String: Any]> {
        return Promise { Resolver in

            Alamofire.request(url)
                .responseRSS { response in
                    //print("Request: \(String(describing: response.request))")   // original url request
                    //print("Response: \(String(describing: response.response))") // http url response
                    //print("Result: \(response.result)")                         // response serialization result
                    
                    switch response.result {
                    case .success:
                        guard let feed: RSSFeed = response.result.value  else {
                            Resolver.fulfill(
                                ["status" : "-1",
                                 "msg" : "Cannot convert to RSS format" ,
                                 "feeds" : []
                                ])
                            return
                        }
                        Resolver.fulfill(
                            ["status" : "1",
                             "msg" : "Success to get rss feed" ,
                             "feeds" : feed.items
                            ])
                    case .failure(let error):
                        
                        Resolver.fulfill( [
                            "status" : String (response.response?.statusCode ?? 1001),
                            "msg" :error.localizedDescription,
                            "feeds" : []
                        ])
                        print(error)
                    }
                    
            }
            
        }
        
    }
    
    
    
    
    /*
    static func request(url: String, completion: @escaping (_ result: String )->Void ) {
        Alamofire.request(url).responseJSON { response in
            print("Request: \(String(describing: response.request))")   // original url request
            print("Response: \(String(describing: response.response))") // http url response
            print("Result: \(response.result)")                         // response serialization result
            
            if let json = response.result.value {
                print("JSON: \(json)") // serialized json response
            }
            
            if let data = response.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)") // original server data as UTF8 string
                completion(utf8Text)
            }
        }
    }*/
    
}
