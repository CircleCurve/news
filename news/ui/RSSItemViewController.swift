//
//  ViewController.swift
//  news
//
//  Created by Patrick Ng on 19/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//
import os.log
import UIKit

class RSSItemViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var urlTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    var rss: RLM_RSS?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        urlTextField.delegate = self
        titleTextField.delegate = self
        descriptionTextField.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        
        loadRSS(feed: rss)
    }
    //MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard.
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let title = titleTextField.text {
            navigationItem.title = title
        }
        updateSaveButtonState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        guard let url = urlTextField.text else {
            //show error
            return
        }
        
        guard let title = titleTextField.text else {
            //show error
            return
        }
        
        guard let desc = descriptionTextField.text else {
            //show error
            return
        }
        
        print ("filter verify")
        let status =  Verify.filter([
            "url" : [url],
            "title" : [title],
            "desc" : [desc]
            ])
        
        print("before save data")
        if status == true {
            if let feed = rss {
                RSS.update(feed: feed, [
                        "link": url,
                        "title": title,
                        "desc": desc
                    ])
            }else{
                rss = RSS.save(link: url, title: title, desc: desc)
            }
            print("finish save data")
        }

    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        let isPresentingInAddMode = presentingViewController is UINavigationController
        if isPresentingInAddMode {
            dismiss(animated: true, completion: nil)
        }else if let owningNavigationController = navigationController {
            owningNavigationController.popViewController(animated: true)
        }else{
            fatalError("The RSSItemViewController is not inside a navigation controller.")
        }
        
    }
    
    //MARK: Private Methods
    
    private func loadRSS (feed: RLM_RSS?) {
        if let feed = feed {
            urlTextField.text = feed.link
            titleTextField.text = feed.title
            descriptionTextField.text = feed.desc
            navigationItem.title = feed.title
        }
    }
    
    private func updateSaveButtonState () {
        // Disable the Save button if the text field is empty.
        let urlText = urlTextField.text ?? ""
        let titleText = titleTextField.text ?? ""
        saveButton.isEnabled = !urlText.isEmpty && !titleText.isEmpty
    }
    
}

