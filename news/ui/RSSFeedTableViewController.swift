//
//  RSSFeedTableViewController.swift
//  news
//
//  Created by Patrick Ng on 29/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//

import UIKit
import PromiseKit
import AlamofireRSSParser
import JGProgressHUD
import Kingfisher
import ImageIO



class RSSFeedTableViewController: UITableViewController {

    var rssItems = [RSSItem]()
    var cellHeightCache = [Int: CGFloat]()
    var rssImageViews = [UIImageView]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.tableView.estimatedRowHeight = 390
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        var links = [String]()
        let network = Networking()
        
        for rss in RSS.all() {
            links.append(rss.link)
        }
        
        print(links)
        
        // Clear memory cache right away.
        ImageCache.default.clearMemoryCache()
        
        // Clear disk cache. This is an async operation.
        ImageCache.default.clearDiskCache()
        
        // Clean expired or size exceeded disk cache. This is an async operation.
        ImageCache.default.cleanExpiredDiskCache()
        
        if links.count > 0 {
            //let link = links[0]
            let network = Networking()
            
            
            var chain = firstly {
                network.requestChain(url: "")
            }
            for link in links {
                
                chain = chain.then{ results -> Promise<[String:Any]> in
                    self.rssItems += results["feeds"] as! [RSSItem]
                    return network.requestChain(url: link)
                }
            }
            
            let _ = chain.done{ results  in
                self.rssItems += results["feeds"] as! [RSSItem]
                
                //rearrange rss based on pubDate
                
                
                self.rssItems.sort(by: {
                    if let aPubDate = $0.pubDate,
                        let bPubDate = $1.pubDate,
                        aPubDate > bPubDate {
                        return true
                    }else {
                        return false
                    }
                })

                
                for (index, rssItem) in self.rssItems.enumerated() {
                    
                    let placeholder = UIImage(named: "image-1920x1080_2x")
                    let imageView = UIImageView()
                    
                    if let imageFromDescription = rssItem.imagesFromDescription, imageFromDescription.count > 0  {
                        
                        //let imageView = UIImageView()
                        imageView.kf.setImage(with: NSURL(string: rssItem.imagesFromDescription![0])! as URL, placeholder : placeholder, completionHandler: {
                            (image, error, cacheType, imageUrl) in
                            if  let _ = image  {
                                print("reload data : imageUrl : \(imageUrl)")
                                //self.tableView.reloadData()
                                self.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                            }
                        })
                    }
                    self.rssImageViews.append(imageView)
                }
                self.tableView!.reloadData()
            }
            
        }
        /*
        RSS.fetch(urls: links, completion: { results in
            self.rssItems =  results as! [RSSItem]
            self.tableView!.reloadData()
            
            }
            
        )*/
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rssItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RSSFeedTableViewCell", for: indexPath) as? RSSFeedTableViewCell else{
            fatalError("The dequeued cell is not an instance of RSSFeedTableViewCell.")
        }
        //print("cell for indexpath.row : \(indexPath.row)")
        let rss = rssItems[indexPath.row]
        cell.titleLabel.text = rss.title
        //cell.imageLabel.image = UIImage(named: "classkit-128x128_2x")
        cell.sourceLabel.text = URL(string: rss.link!)?.host

        cell.dateLabel.text = rss.pubDate?.toString(dateFormat: "yyyy-MM-dd") ?? ""
        
        
        //cell.imageLabel.image = nil
        let isIndexValid = self.rssImageViews.indices.contains(indexPath.row)
        if isIndexValid == true {
            //cell.imageLabel.downloadImageFrom(link: imageFromDescription[0], contentMode: UIViewContentMode.scaleAspectFill)
            cell.imageLabel.image = self.rssImageViews[indexPath.row].image

        }else {
            cell.imageLabel.image = nil
        }
        
        
        return cell
    }

    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
