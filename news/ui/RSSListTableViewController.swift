//
//  RSSListTableViewController.swift
//  news
//
//  Created by Patrick Ng on 26/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//
import os.log
import UIKit
import RealmSwift

class RSSListTableViewController: UITableViewController {

    var rssItems = [RLM_RSS]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.navigationItem.leftBarButtonItem = self.editButtonItem
        rssItems = loadRSSList()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.leftBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rssItems.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reuseIdentifier = "RSSListTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)   else {
            // Never fails:
            print("fail")
            return UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: reuseIdentifier)
        }
        
        let rssItem = rssItems[indexPath.row]
        cell.textLabel?.text = rssItem.title
        cell.detailTextLabel?.text = rssItem.desc

        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            RSS.remove(feed: rssItems[indexPath.row])
            
            rssItems.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        super.prepare(for: segue, sender: sender)
        switch (segue.identifier ?? "") {
        case "AddRSSItem":
            os_log("Adding a new rss feed", log: OSLog.default, type:.debug)
        
        case "ShowRSSDetail":
            guard let rssItemViewController = segue.destination as? RSSItemViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            
            guard let selectedRSSCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            
            guard let indexPath = tableView.indexPath(for: selectedRSSCell) else{
                fatalError("The selected cell is not being displayed by the table")
            }
            
            let selectCell = rssItems[indexPath.row]
            rssItemViewController.rss = selectCell
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
        
    }
    
    //MARK: Actions
    @IBAction func unwindToRSSList(sender: UIStoryboardSegue){
        if let sourceViewController = sender.source as? RSSItemViewController,  let rss = sourceViewController.rss{
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                //Update existing rss
                rssItems[selectedIndexPath.row] = rss
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }else{
                //Add a new rss
                let newIndexPath = IndexPath(row: rssItems.count, section: 0)
                rssItems.append(rss)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            
        }
        
    }
    

    //MARK: Private Methods
    private func loadRSSList() -> [RLM_RSS] {
        
        return RSS.all()
    }
}
