//
//  newsTests.swift
//  newsTests
//
//  Created by Patrick Ng on 19/3/2018.
//  Copyright © 2018 Patrick. All rights reserved.
//

import XCTest
import RealmSwift
@testable import news


class newsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    //MARK: test network
    func testNetworkCanSuccessReturnResult () {
        let expectation = self.expectation(description: "fetch rss feed from itunes")

        let url = "https://developer.apple.com/news/rss/news.rss"
        //Networking.request(url: url, completion: { ( results : [Any] ) in
        
        Networking.request(
            url: url,
            onSuccess: { status, msg, results in
                print("success to call rss")
                for result in results {
                    print (result)
                }
                expectation.fulfill()
            },
            onFail: { status, msg in
                print("fail to call rss, status:\(status), msg:\(msg)")
            },
            onError: { status, msg in
                print("network error to call rss, status:\(status) msg:\(msg)")
            }
        )
        self.waitForExpectations(timeout: 5.0, handler: nil)

    }
    
    func testNetworkFailToGetResult () {
        let expectation = self.expectation(description: "fetch rss feed from itunes")
        
        let url = "https://developer.apple.com/news/rss/news1.rss"
        
        Networking.request(
            url: url,
            onSuccess: { status, msg, results in
                print("success to call rss")
                for result in results {
                    print (result)
                }
                expectation.fulfill()
            },
            onFail: { status, msg in
                print("fail to call rss, status:\(status), msg:\(msg)")
                expectation.fulfill()
            },
            onError: { status, msg in
                print("network error to call rss, status:\(status) msg:\(msg)")
            }
        )
        self.waitForExpectations(timeout: 5.0, handler: nil)
        
    }
    
    func testNetworkError () {
        let expectation = self.expectation(description: "fetch rss feed from itunes")
        
        let url = "https://developer222.apple.com/news/rss/news222.rss"
        
        Networking.request(
            url: url,
            onSuccess: { status, msg, results in
                print("success to call rss")
                for result in results {
                    print (result)
                }
        },
            onFail: { status, msg in
                print("fail to call rss, status:\(status), msg:\(msg)")
        },
            onError: { status, msg in
                print("network error to call rss, status:\(status) msg:\(msg)")
                expectation.fulfill()
        }
        )
        self.waitForExpectations(timeout: 5.0, handler: nil)
        
    }
    
    func testAddSomethingIntoDatabaseSuccess() {
        let realm = try! Realm()
        let rssItem = RLM_RSS()
        rssItem.link = "https://developer.apple.com/news/rss/news.rss"
        rssItem.title = "蘋果開發人員網站"
        rssItem.desc = "You can view the most latest news in apple"
        
        try! realm.write {
            realm.add(rssItem)
            print ("DB File save in \(Realm.Configuration.defaultConfiguration.fileURL!)")
        }
        
        try! realm.write {
            realm.delete(rssItem)
        }
    }
    
    func testAddSomethingNilIntoDatabaseSuccess() {
        let realm = try! Realm()
        let rssItem = RLM_RSS()
        rssItem.link = nil ?? ""
        rssItem.title = nil ?? ""
        rssItem.desc = nil ?? ""
        
        
        try! realm.write {
            realm.add(rssItem)
            print ("DB File save in \(Realm.Configuration.defaultConfiguration.fileURL!)")
        }
        
        try! realm.write {
            realm.delete(rssItem)
        }
    }
    
    func testGetSomethingInDatabase() {
        let realm = try! Realm()
        let testRssItem = RLM_RSS()
        testRssItem.link = "https://developer.apple.com/news/rss/news.rss"
        testRssItem.title = "蘋果開發人員網站"
        testRssItem.desc = "You can view the most latest news in apple"

        try! realm.write() {
            realm.add(testRssItem)
        }
        let rssItem = realm.objects(RLM_RSS.self)
        XCTAssert(rssItem.count > 0, "rssItem > 0")
        XCTAssert(rssItem[0].link == testRssItem.link , "rssItem.link is identical to testRssItem.link")
        
        try! realm.write() {
            realm.delete(rssItem)
        }
        
    }
    
    func testCanUpdateInformationSuccessfully() {
        let realm = try! Realm()
        let testRssItem = RLM_RSS()
        testRssItem.link = "https://developer.apple.com/news/rss/news.rss"
        testRssItem.title = "蘋果開發人員網站"
        testRssItem.desc = "You can view the most latest news in apple"
        
        try! realm.write() {
            realm.add(testRssItem)
        }
        
        if let rssItem = realm.objects(RLM_RSS.self).filter("title='蘋果開發人員網站'").first {
            try! realm.write {
                rssItem.title = "蘋果開發人員網站(更新)"
            }
        }
    }
    
    func testDeleteSomethingIntoDatabaseSuccess() {
        let realm = try! Realm()
        let rssItem = RLM_RSS()
        rssItem.link = "https://developer.apple.com/news/rss/news.rss"
        rssItem.title = "蘋果開發人員網站"
        rssItem.desc = "You can view the most latest news in apple"

        
        try! realm.write {
            realm.delete(rssItem)
            print ("Delete success. DB File save in \(Realm.Configuration.defaultConfiguration.fileURL!)")
        }
    }
    
    func testCanFilterString() {

        let status = Verify.filter ( [
                "url" : [
                    "https://developer.apple.com/news/rss/news.rss",
                ],
                "title": [ "蘋果開發人員網站" ],
                "desc": ["You can view the most latest news in apple"]
            ]
        )
        XCTAssert(status, "all data format is right")
    }
    
    func testCanSaveRSSFeed () {
        RSS.save(
            link: "https://developer.apple.com/news/rss/news.rss",
            title: "蘋果開發人員網站",
            desc: "You can view the most latest news in apple"
        )
        
    }
    
    

    

}
